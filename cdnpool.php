<?
class Cdnpool {
    private $redis;
    // private $redishosts = array("10.18.19.98", "10.18.19.200");
    private $redishosts = array("lbrdhost");
    private $redisport = 6383;
    private $redisdb = 8;

    public function __construct () {
        $this->redis = new Redis();
    }

    public function setpool ($json_string) {
        $json_array = json_decode($json_string, true);
        $return = true;
        try {
            foreach($this->redishosts as $redis) {
                $this->redis->connect($redis, $this->redisport, 0.5);
                $this->redis->select($this->redisdb);
                if (!empty($json_array['ttl'])) $this->redis->pSetEx($json_array['group_no'], $json_array['ttl'], json_encode(@$json_array['list_host']));
                else $this->redis->set($json_array['group_no'], json_encode(@$json_array['list_host']));
                // $this->redis->set($json_array['group_no'], json_encode($json_array['list_host']));
                $this->redis->close();
                file_put_contents("/www/logs/commonapi/cdnpool-acc_". date("Ymd_H", time()) .".log", date("Y-m-d_H:i:s", time())."  ".$redis.":".$this->redisport.":".$this->redisdb."  SET ".$json_array['group_no']." ".json_encode(@$json_array['list_host'])." ".$json_array['ttl']."\n", FILE_APPEND);
            }
        } catch (Exception $e) {
            $return = false;
            file_put_contents("/www/logs/commonapi/cdnpool-err". date("Ymd_H", time()) .".log", date("Y-m-d_H:i:s", time())."  ".serialize($redis).":".$this->redisport.":".$this->redisdb."  ".$e->getMessage()."\n", FILE_APPEND);
            notify("error", $this->redishost.":".$this->redisport.":".$this->redisdb."  ".$e->getMessage());
            newrelic_notice_error(serialize($redis).":".$this->redisport.":".$this->redisdb."  ".$e->getMessage());
        }
        return $return;
    }

    public function main () {
        $json_string = file_get_contents("php://input");
        return $this->setpool($json_string);
    }
}

function notify ($mode, $details) {
    date_default_timezone_set("Asia/Bangkok");
    $timestamp = date("Y:m:d-H:i:s");
    if (is_array($details)) $details = implode("  ", $details);
    $details = $timestamp ."  ". __FILE__ ." ". gethostname() ."  ". $details;
  
    $sToken = "gbaX513e4nUrGQu3QAiFONd67gSx1X8pOOKewt9HdfS";

    $chOne = curl_init(); 
    curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt( $chOne, CURLOPT_PROXY, "10.18.83.178:3128");
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt( $chOne, CURLOPT_POST, 1);
    curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$details);
    $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
    curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
    curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec( $chOne );

    //Result error
    //if(curl_error($chOne)) file_put_contents(__DIR__."/logs/cdnpool-notify-err_".date("Ymd_H").".log", date("Y/m/d H:i:s")."  ".'error:' . curl_error($chOne)."\n", FILE_APPEND);
    curl_close( $chOne );
}

$cdnpool = new Cdnpool();
if ($cdnpool->main() === true) print(json_encode(array("result" => 200, "description" => "SUCCESS")));
else print(json_encode(array("result" => 500, "description" => "Something wents wrong.")));
