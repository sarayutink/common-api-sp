<?php
// side effect
ini_set('error_reporting', E_ERROR);

// declaration
define("RDHOST", "sprdhost");
define("RDPORT", 6380);
define("RDDB", 4);

define("LOGDIR", "/www/logs/commonapi");

function main ($post) {
  $logsparam = array(date("Y:m:d-H:i:s"), RDHOST.":".RDPORT."[".RDDB."]", serialize($post));
  // writeLogs("param", $logsparam, FILE_APPEND);

  try {
    if (setRedis($post)) {
        writeLogs("access", $logsparam);
        echo "SUCCESS";
    }
    else {
        writeLogs("access", $logsparam);
        echo "FAIL";
    }
  }
  catch (Exception $e) {
      $logsparam[] = $e->getMessage();
      writeLogs("error", $logsparam);
      newrelic_notice_error($e);
      echo "FAILED";
  }
}

function setRedis ($post) {
  $redis = new Redis();
  $redis->connect(RDHOST, RDPORT);
  $redis->select(RDDB);
  $return = false;
  if (isset($post['assetid'])) $return = $redis->set($post['assetid'], "last-idol");
  $redis->close();

  return $return;
}

function writeLogs ($mode, $details) {
  $timestamp = date("Y:m:d-H:i:s");
  if (is_array($details)) $details = implode("  ", $details);
  $details = $timestamp . $details;
  file_put_contents(LOGDIR."/setassetid_".date("Ymd_H")."_".$mode.".log", $details ."\n", FILE_APPEND);
}

main($_POST);
?>
