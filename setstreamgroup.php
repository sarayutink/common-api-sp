<?php
// side effect
ini_set('error_reporting', E_ERROR);

// declaration
define("RDHOST", "bzrdhost");
define("RDPORT", 6379);
define("RDDB", 5);

define("LOGDIR", __DIR__ ."/logs");

function main ($body) {
  $bodyArray = json_decode($body, true);

  $logsparam = array(date("Y:m:d-H:i:s"), RDHOST.":".RDPORT."[".RDDB."]", $bodyArray['groupID'], $bodyArray['max'], $bodyArray['count'], count($bodyArray['streamNames']));
  // writeLogs("param", $logsparam, FILE_APPEND);

  try {
    if (setRedis($bodyArray)) {
	// writeLogs("access", $logsparam);
	echo "SUCCESS";
    }
    else {
	writeLogs("access", $logsparam);
	echo "FAIL";
    }
  }
  catch (Exception $e) {
      $logsparam[] = $e->getMessage();
      writeLogs("error", $logsparam);
      newrelic_notice_error($e);
      echo "FAILED";
  }
}

function setRedis ($bodyArray) {
  $redis = new Redis();
  $redis->connect(RDHOST, RDPORT);
  $redis->select(RDDB);
  $return = false;
  foreach ($bodyArray['streamNames'] as $streamname) {
    // $return = $redis->setEx($streamname, 70, $bodyArray['groupID']. "|". $bodyArray['max']);
    $return = $redis->set($streamname, $bodyArray['groupID']. "|". $bodyArray['max']);
    if ($return === false) break;
  }

  // $iterator = null;
  // while (false !== ($keys = $redis->scan($iterator, ""))) {
  //   foreach($keys as $key) {
  //     $value = $redis->get($key);
  //     echo "key : $key, value : $value \n";
  //     if ($bodyArray['groupID'] == explode("|", $value)[0] && !in_array($key, $bodyArray['streamNames'])) {
  //       echo "delete key : $key, value : $value \n";
  //     }
  //   }
  // }
  $redis->close();

  return $return;
}

function writeLogs ($mode, $details) {
  $timestamp = date("Y:m:d-H:i:s");
  if (is_array($details)) $details = implode("  ", $details);
  $details = $timestamp . $details;
  file_put_contents(LOGDIR."/setassetid_".date("Ymd_H")."_".$mode.".log", $details ."\n", FILE_APPEND);
}

function unzipBody ($body) {
  $return = gzdecode($body);
  if ($return === false) $return = $body;
  return $return;
}

$body = unzipBody(file_get_contents("php://input"));
main($body);
// setRedis(null);
?>
