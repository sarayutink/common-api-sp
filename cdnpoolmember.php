<?
define("RDHOST", "10.18.19.98");
define("RDPORT", 6383);
define("RDDB", 8);

$redis = new Redis();
try {
    $redis->connect(RDHOST, RDPORT);
    $redis->select(RDDB);
    $groups = $redis->keys("*");
    foreach ($groups as $group) {
        $ttl = $redis->ttl($group);
        // echo "ttl $group = $ttl\n";
        if ($ttl > 0 && $ttl <= 120) {
            $members = $redis->get($group);
            $redis->persist($group);
            // echo "get $group = $members\n";
            if ($members != null) notify($group, $members);
        }
    }
    $redis->close();
}
catch (Exception $e) {
    echo $e->getMessage();
}

function notify ($group, $members) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    date_default_timezone_set("Asia/Bangkok");

    $sToken = "gbaX513e4nUrGQu3QAiFONd67gSx1X8pOOKewt9HdfS";
    $sMessage = "Group $group has not updated for a while.\n";
    $sMessage .= "Automatic remove expire of [ $group ] with [ ";
    foreach (json_decode($members, true) as $member) $sMessage .= $member['host']." ";
    $sMessage .= "]";

    $chOne = curl_init(); 
    curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
    curl_setopt( $chOne, CURLOPT_PROXY, "10.18.19.151:80");
    curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
    curl_setopt( $chOne, CURLOPT_POST, 1); 
    curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$sMessage); 
    $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
    curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
    curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
    $result = curl_exec( $chOne ); 

    //Result error 
    if(curl_error($chOne)) file_put_contents(__DIR__."/logs/cdnpool-notify-err_".date("Ymd_H").".log", date("Y/m/d H:i:s")."  ".'error:' . curl_error($chOne)."\n", FILE_APPEND);
    curl_close( $chOne );
}