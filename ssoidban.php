<?php

$file = 'log_ssoidban.txt';
$log = file_get_contents($file);

date_default_timezone_set('Asia/Bangkok');
$time = date("Y-m-d H:i:s");
$timelog = date("[d/M/Y:H:i:s O]");

    $redis_server1 = "10.18.19.96";
    $redis_server2 = "10.18.19.98";
    $redis_server3 = "10.18.19.126";
    $redis_server4 = "10.18.19.200";
    $redis_server5 = "10.28.147.68";
    $redis_server6 = "10.28.19.98";
    $redis_server7 = "10.18.19.101";

    // $redis_server_local = "10.18.19.199";
    $redis_server_local = "10.28.20.60";
    $redis_port = 6380;

    $server_redis = array($redis_server1,$redis_server2,$redis_server3,$redis_server4,$redis_server5,$redis_server6);
    $server_local = array($redis_server_local,$redis_server7);
    $redis_number = 7;
    $redis_lotid = 8;

    $redis1 = new Redis();
    $red1 = $redis1->connect($redis_server1,$redis_port);
    $redis2 = new Redis();
    $red2 = $redis1->connect($redis_server2,$redis_port);
    $redis3 = new Redis();
    $red3 = $redis3->connect($redis_server3,$redis_port);
    $redis4 = new Redis();
    $red4 = $redis4->connect($redis_server4,$redis_port);
    $redis5 = new Redis();
    $red5 = $redis5->connect($redis_server5,$redis_port);
    $redis6 = new Redis();
    $red6 = $redis6->connect($redis_server6,$redis_port);

    $redislocal = new Redis();
    $redlocal = $redislocal->connect($redis_server_local,$redis_port);

    $method = $_SERVER['REQUEST_METHOD'];

    if(($red1 && $red2 && $red3 && $red4 && $red5 && $red6) == true){


######################################### POST #########################################

        if('POST' === $method){
            $rawdata = file_get_contents("php://input");
            $data = (array)(json_decode($rawdata));
            if($data['ssoid'] && $data['account'] != null){
                $redis = new Redis();
                $redis->connect($redis_server1,$redis_port);      //อย่าลืมเปลี่ยน redis server จาก redis_server_local เป็น redis_server1
                $redis->select($redis_number);
                $allKeys = $redis->keys('*');
                $redis->close();
                if($allKeys != null){
                    foreach($server_redis as $server){                 //อย่าลืมเปลี่ยน redis server จาก server_local เป็น server_redis
                        $setredis7 = new Redis();
                        $setredis7->connect($server,$redis_port);
                        $setredis7->select($redis_number);
                        $setredis8 = new Redis();
                        $setredis8->connect($server,$redis_port);
                        $setredis8->select($redis_lotid);
                        $valuelotid = array();
                        $banned = array();
                        $banneddup = array();
                        foreach($data['ssoid'] as $ssoid){
                            if(!in_array($ssoid,$allKeys)){
                                $setdata = array("ssoid" => $ssoid,"userID" => $data['account'],"time" => $time);
                                $banned[] = array("ssoid" => $ssoid,"userID" => $data['account'],"time" => $time);
                                $valuelotid[] = $setdata;
                                $setredis7->set($ssoid,serialize($setdata)); //แบนถาวร
                            }else{
                                $banneddup[] = unserialize($setredis7->get($ssoid));
                                $dup[] = $ssoid;
                            }
                        }
                        if($valuelotid != null){
                            $setredis8->set(date("YmdHis",strtotime($time)),serialize($valuelotid)); //แบนถาวร
                        }
                        $setredis7->close();
                        $setredis8->close();
                    }
                    foreach($banned as $set){
                        $log = $timelog ." ". $method ." ".$set['ssoid']." ". $data['account'] ." ". date("YmdHis",strtotime($set['time'])) ." ". "SUCCESS"."\n";
                        file_put_contents($file,$log, FILE_APPEND | LOCK_EX);
                    }
                    foreach($banneddup as $set){
                        $log = $timelog ." ". $method ." ".$set['ssoid']." ". $data['account'] ." ". date("YmdHis",strtotime($set['time'])) ." ". "SSOID DUPLICATE"."\n";
                        file_put_contents($file,$log, FILE_APPEND | LOCK_EX);
                    }
                    $ssoid_suc['count'] = count($valuelotid);
                    $ssoid_dup = array("count" => count($dup),"row" => $dup);
                }else{
                    foreach($server_redis as $server){                 //อย่าลืมเปลี่ยน redis server จาก server_local เป็น server_redis
                        $setredis7 = new Redis();
                        $setredis7->connect($server,$redis_port);
                        $setredis7->select($redis_number);
                        $setredis8 = new Redis();
                        $setredis8->connect($server,$redis_port);
                        $setredis8->select($redis_lotid);
                        $valuelotid = array();
                        $banned = array();
                        foreach($data['ssoid'] as $ssoid){
                            $setdata = array("ssoid" => $ssoid,"userID" => $data['account'],"time" => $time);
                            $banned[] = array("ssoid" => $ssoid,"userID" => $data['account'],"time" => $time);
                            $valuelotid[] = $setdata;
                            $setredis7->set($ssoid,serialize($setdata)); //แบนถาวร
                        }
                        $setredis8->set(date("YmdHis",strtotime($time)),serialize($valuelotid)); //แบนถาวร
                        $setredis7->close();
                        $setredis8->close();
                    }
                    foreach($banned as $set){
                        $log = $timelog ." ". $method ." ".$set['ssoid']." ". $data['account'] ." ". date("YmdHis",strtotime($set['time'])) ." ". "SUCCESS"."\n";
                        file_put_contents($file,$log, FILE_APPEND | LOCK_EX);
                    }
                    $ssoid_suc['count'] = count($data['ssoid']);
                    $ssoid_dup = array("count" => 0,"row" => array());
                }
                $array['account'] = $data['account'];
                $array['success'] = $ssoid_suc;
                $array['duplicate'] = $ssoid_dup;
                echo json_encode($array);
                header('Content-Type: application/json');
            }

#########################################  GET #########################################

        }else if('GET' === $method){
            $rawdata = file_get_contents("php://input");
            $data = (array)(json_decode($rawdata));
            if($data['lotid'] == "all"){
                $redis = new Redis();
                $redis->connect($redis_server1,$redis_port);    //อย่าลืมเปลี่ยน redis server จาก redis_server_local เป็น redis_server1 
                $redis->select($redis_lotid);
                $allKeys = $redis->keys('*');
                rsort($allKeys);
                $redis->close();
                $array['count'] = count($allKeys);
                $array['lotid'] = $allKeys;
                header('Content-Type: application/json');
                echo json_encode($array);
            }else if($data['lotid'] != null){
                $rawdata = file_get_contents("php://input");
                $data = (array)(json_decode($rawdata));
                $redis = new Redis();
                $redis->connect($redis_server1,$redis_port);    //อย่าลืมเปลี่ยน redis server จาก redis_server_local เป็น redis_server1 
                $redis->select($redis_lotid);
                $lotid = unserialize($redis->get($data['lotid']));
                $array['count'] = count($lotid);
                $array['row'] = $lotid;
                $redis->close();
                header('Content-Type: application/json');
                echo json_encode($array);
            }else{
                $redis = new Redis();
                $redis->connect($redis_server1,$redis_port);    //อย่าลืมเปลี่ยน redis server จาก redis_server_local เป็น redis_server1 
                $redis->select($redis_number);
                $allKeys = $redis->keys('*');
                sort($allKeys);
                foreach($allKeys as $ssoid){
                    $value = unserialize($redis->get($ssoid));
                    $ssoidall[] = $value;
                }
                $array['IP'] = $ip;
                $array['count'] = count($ssoidall);
                $array['banned'] = $ssoidall;
                header('Content-Type: application/json');
                echo json_encode($array);
                $redis->close();
            }

######################################  DELETE #########################################

        }else if('DELETE' === $method){          
            $rawdata = file_get_contents("php://input");
            $data = (array)(json_decode($rawdata));
            $redis = new Redis();
            $redis->connect($redis_server1,$redis_port);     //อย่าลืมเปลี่ยน redis server จาก redis_server_local เป็น redis_server1
            $redis->select($redis_number);
            $allKeys = $redis->keys('*');
            sort($allKeys);
            $redis->close();
            if($allKeys != null){
             
                foreach($server_redis as $server){                //อย่าลืมเปลี่ยน redis server จาก server_local เป็น server_redis
                    $delredis7 = new Redis();
                    $delredis7->connect($server,$redis_port);
                    $delredis7->select($redis_number);
                    $delredis8 = new Redis();
                    $delredis8->connect($server,$redis_port);
                    $delredis8->select($redis_lotid);
                    $ssoidnotfound = array();
                    $deleted = array();
                    foreach($data['ssoid'] as $ssoid){
                        if(in_array($ssoid,$allKeys)){
                            //$deleted[] = $ssoid;
                            $deleted[] = unserialize($delredis7->get($ssoid));
                            $value = unserialize($delredis7->get($ssoid));
                            $delredis7->delete($ssoid);

                            $valuelotid = unserialize($delredis8->get(date("YmdHis",strtotime($value['time']))));
                            foreach($valuelotid as $key => $va){
                                if($value === $va){
                                    unset($valuelotid[$key]);
                                }
                            }
                            if($valuelotid != null){
                                $delredis8->delete(date("YmdHis",strtotime($value['time'])));
                                $delredis8->set(date("YmdHis",strtotime($value['time'])),serialize($valuelotid)); //แบนถาวร
                                
                            }else{
                                $delredis8->delete(date("YmdHis",strtotime($value['time'])));
                            }
                        }else{
                            $ssoidnotfound[] = $ssoid;
                        }
                    }
                    $delredis7->close();
                    $delredis8->close();
                }
                foreach($deleted as $del){
                    $log = $timelog ." ". $method ." ".$del['ssoid']." ". $data['account'] ." ". date("YmdHis",strtotime($del['time'])) ." ". "SUCCESS"."\n";
                    file_put_contents($file,$log, FILE_APPEND | LOCK_EX);
                }
                foreach($ssoidnotfound as $notf){
                    $log = $timelog ." ". $method ." ".$notf." ". $data['account'] ." "."-"." "."SSOID NOT FOUND"."\n";
                    file_put_contents($file,$log, FILE_APPEND | LOCK_EX);
                }
                $ssoid_del = array("count" => count($deleted) , "row" => $deleted);
                $ssoid_not = array("count" => count($ssoidnotfound),"row" => $ssoidnotfound);
            }
            $array['account'] = $data['account'];
            $array['deleted'] = $ssoid_del;
            $array['not found'] = $ssoid_not;
            echo json_encode($array);
            header('Content-Type: application/json');
        }
    }else{
        $array['redis'] = "Redis connection is failed";
        echo json_encode($array);
        // notify(@$server);
        header('Content-Type: application/json');
    }

    function notify ($redis) {
        date_default_timezone_set("Asia/Bangkok");
    
        $sToken = "gbaX513e4nUrGQu3QAiFONd67gSx1X8pOOKewt9HdfS";
        $sMessage = "Redis $redis connection failed.";
    
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_PROXY, "10.18.19.151:80");
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        curl_setopt( $chOne, CURLOPT_POST, 1); 
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$sMessage); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
    
        //Result error 
        // if(curl_error($chOne)) file_put_contents(__DIR__."/logs/cdnpool-notify-err_".date("Ymd_H").".log", date("Y/m/d H:i:s")."  ".'error:' . curl_error($chOne)."\n", FILE_APPEND);
        curl_close( $chOne );
    }
?>