<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Network_model extends CI_Model {
    private $host;
    private $port;
    private $db;
    
    private $redis;

    public function __construct () {
        $this->redis = new Redis();

        $this->host = array("lbrdhost");
        $this->port = 6383;
        $this->db = 5;
    }

    public function setNetwork ($node, $setsarray, $msetarray) {
        $add = $this->setSets($node, $setsarray, $msetarray);
        if ($add['result'] == true) return array('result' => true, 'description' => "success");
        else return $add;
    }

	private function setSets ($node, $setsarray, $msetarray) {
        try {
            $this->redis->connect($this->host, $this->port);
            $this->redis->select($this->db);

            $this->redis->multi();
            ## remove node's subnet
            $members = $this->redis->sMembers($node);
            $this->redis->del($members);
            ## set input subnet
            $res = $this->redis->mset($msetarray);
            ## create sets of node
            $count = $this->redis->sAdd($node, $setsarray);
            $this->redis->exec();

            $this->redis->close();

            if ($res === false && count($setsarray) == $count) return array('result' => false, 'description' => "set to redis failed.");
            return array('result' => true, 'description' => "success");
        }
        catch (Exception $e) {
            return array('result' => false, 'description' => $e->getMessage() ."/". $e->getTraceAsString());
        }
    }
}
