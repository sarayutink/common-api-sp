<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livemap_model extends CI_Model {
    private $hosts;
    private $port;
    private $db;
    
    private $redis;

    public function setParams () {
        $this->redis = new Redis();

        $this->hosts = "sprdhost";
        $this->port = 6380;
        $this->db = 1;
    }

	public function setArray ($array, $appid, $type, $visitors) {
        try {
                $this->redis->connect($this->hosts, $this->port);
                $this->redis->select($this->db);

                ## get old livemapping
                $keys = array();
                foreach($visitors as $visitor) $keys = array_merge($keys, $this->redis->keys($appid."*".$type."*".$visitor."*"));

                $this->redis->multi();
                ## del old livemapping
                if (is_array($keys)) $res = $this->redis->del($keys);
                else $res = true;
                ## set new livemapping
                foreach($array as $key => $val) $res = $this->redis->set($key, $val);
                $this->redis->exec();

                $this->redis->close();

                if ($res === false) return array('result' => false, 'description' => "redis failed.");
            return array('result' => true, 'description' => "success");
        }
        catch (Exception $e) {
            return array('result' => false, 'description' => $e->getMessage() ."/". $e->getTraceAsString());
        }
	}
}
