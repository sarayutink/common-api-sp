<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdnprofile_model extends CI_Model {
    private $redis;
    private $hosts;
    private $port;
    private $db;

    public function __construct () {
        parent::__construct();
        $this->redis = new Redis();
        
        $this->hosts = "lbrdhost";
        $this->port = 6383;
        $this->db = array("version" => 0, "profileparam" => 1, "overflowparam" => 2, "profile" => 6, "weight" => 7, "pool" => 8, "overflow" => 9);
    }

    public function getProfileVersion ($json_array) {
        try {
            $this->redis->connect($this->hosts[0], $this->port);
            $this->redis->select($this->db['version']);
            $version = $this->redis->get($json_array['profile_id']);
        }
        catch (Exception $e) {
            return false;
        }
        return $version;
    }

    public function setProfile ($json_array) {
            try {
                $this->redis->connect($this->hosts, $this->port);
                $this->redis->multi();

                $profileScore = $this->getProfileScore($json_array['profile_param']);
                $this->redis->select($this->db['weight']);
                $setWeight = $this->redis->set($json_array['profile_id'], $profileScore);
                // $this->printf_json(array("setWeight" => $setWeight, $json_array['profile_id'] => $profileScore));

                ## remove old profile's params and profile's set
                $this->redis->select($this->db['profileparam']);
                $members = $this->redis->sMembers($json_array['profile_id']);
                foreach($members as $key) $this->redis->sRem($key, $json_array['profile_id']);
                $this->redis->del($json_array['profile_id']);


                foreach($json_array['profile_param'] as $profile_param_array) {
                    $param_default_val = $profile_param_array['val'];
                    // $param_default_val = $this->defaultvalue($profile_param_array['name'], $profile_param_array['val']);
                    // $this->printf_json(array("profile_param_name" => $profile_param_array['name'], "profile_param_val" => $profile_param_array['val'], "param_default_val" => $param_default_val));
                    if (is_array($param_default_val)) {
                        foreach($param_default_val as $val) {
                            $this->redis->select($this->db['profile']);
                            $this->redis->sAdd(($val), $json_array['profile_id']);
                            // $this->printf_json(array("sAdd" => $sAdd, $json_array['profile_id'] => strtolower($val)));
                        }
                    }

                    ## add new profile's params into profile's set
                    $this->redis->select($this->db['profileparam']);
                    $this->redis->sAdd($json_array['profile_id'], $val);
                }


                $this->redis->select($this->db['version']);
                $this->redis->set($json_array['profile_id'], $json_array['version']);

                $this->redis->exec();
                $this->redis->close();
            }
            catch (Exception $e) {
                return array("result" => false, "description" => "redis profile failed.");
            }

        if (isset($json_array['overflow_rule'])) {
            foreach ($json_array['overflow_rule'] as $i => $overflow_rule) {
                $overflow_rule["param"][] = array("name" => "profile_id", "val" => array($json_array['profile_id']));
                // $this->printf_json(array("index" => $i, "overflow_rule" => $overflow_rule));
                $result = $this->setOverflow($overflow_rule, $json_array['profile_id']);
                // $this->printf_json(array("profile_id" => $overflow_rule["profile_id"], "profileScore" => $profileScore));
            }
            // $this->printf_json(array("overflow_rule" => $json_array['overflow_rule'], "profile_param" => $json_array['profile_param']));
            // $overflow_result = $this->setOverflow($json_array['overflow_rule'], $json_array['profile_id']);
            // if ($overflow_result['result'] == false) return $overflow_result;
        }

        return $this->setPool($json_array);
    }

    public function setOverflow ($overflow_rule, $parent_id) {
            try {
                $this->redis->connect($this->hosts, $this->port);
                $this->redis->multi();

                ## remove old profile's params and profile's set
                $this->redis->select($this->db['overflowparam']);
                $members = $this->redis->sMembers($overflow_rule['profile_id'].$parent_id);
                foreach($members as $key) $this->redis->sRem($key, $overflow_rule['profile_id'].$parent_id);
                $this->redis->del($overflow_rule['profile_id'].$parent_id);

                $profileScore = $this->getProfileScore($overflow_rule['param']);
                $this->redis->select($this->db['weight']);
                $setWeight = $this->redis->set($overflow_rule['profile_id'].$parent_id, $profileScore);
                $this->printf_json(array("setWeight" => $setWeight, $overflow_rule['profile_id'].$parent_id => $profileScore));
                // $this->printf_json(array($overflow_rule['profile_id'] => $profileScore, "param" => $overflow_rule['param']));
                foreach($overflow_rule["param"] as $profile_param_array) {
                    // $this->printf_json(array("host" => $host, "port" => $this->port, "db" => $this->db['overflow']));
                    // $this->printf_json(array("profile_param_name" => $profile_param_array['name'], "profile_param_val" => $profile_param_array['val']));
                    if (is_array($profile_param_array['val'])) {
                        foreach($profile_param_array['val'] as $val) {
                            $this->redis->select($this->db['overflow']);
                            $sAdd = $this->redis->sAdd(strtolower($val), $overflow_rule['profile_id']);
                            // $this->printf_json(array("overflow_sAdd" => $sAdd, $overflow_rule['profile_id'] => strtolower($val)));
                        }
                    }
                    ## add new profile's params into profile's set
                    $this->redis->select($this->db['overflowparam']);
                    $this->redis->sAdd($overflow_rule['profile_id'].$parent_id, $val);
                }

                $this->redis->exec();
                $this->redis->close();
            }
            catch (Exception $e) {
                return array("result" => false, "description" => "redis profile failed.");
            }
        return array("result" => true);
    }

    public function setPool ($json_array) {
        $result = true;
            try {
                $this->redis->connect($this->hosts, $this->port);
                $this->redis->select($this->db['pool']);
                // $result &= $json_array['interval'] > 0 ? $this->redis->set($json_array['profile_id'], json_encode($json_array['list_host']), $json_array['interval']) : $this->redis->set($json_array['profile_id'], json_encode($json_array['list_host']));
                $result = $this->redis->set($json_array['profile_id'], json_encode($json_array['list_host']));
                $result = $this->redis->set($json_array['profile_name'], json_encode($json_array['list_host']));
                $this->redis->close();
            } catch (Exception $e) {
                $result = false;
            }
        if ($result == true) $return = array("result" => true, "description" => "success.");
        else $return = array("result" => false, "description" => "cdnpool redis failed.");
        return $return;
    }

    private function defaultvalue ($paramname, $paramval) {
        $default_map = array("node_code" => "other");
        if (in_array($paramname, array_flip($default_map))) {
            if ($paramval === null) return array($default_map[$paramname]);
            elseif ($paramval === false) return array($default_map[$paramname]);
            else return $paramval;
        }
        else return $paramval;
        // return (!in_array($paramname, array_flip($default_map)) && is_null($paramval) ? $paramval : array($default_map[$paramname]));
    }

    private function getProfileScore ($params) {
        $bitwise = array(
            "profile_id"        => 0b10000000000000,
            "uid"               => 0b00001000000000,
            "sessionid"         => 0b00000100000000,
            "appid"             => 0b01000000000000,
            "channelid"         => 0b00000010000000,
            "streamname"        => 0b00000000100000,
            "type"              => 0b00100000000000,
            "visitor"           => 0b00010000000000,
            "drm"               => 0b00000000010000,
            "node_code"         => 0b00000000000100,
            "streamlvl"         => 0b00000001000000,
            "OPERATOR_BIT"      => 0b00000000001000,
            "LOGIN_BIT"         => 0b00000000000010,
            "WHITE_LIST_BIT"    => 0b00000000000001
        );

        // $this->printf_json(array("params" => $params));
        $score = 0;
        foreach ($params as $param) {
            if (!is_null($param['val']) && $param['val'] !== false) {
                // $this->printf_json(array("param_name" => $param['name']));
                $score += $bitwise[$param['name']];
            }
        }
        // $this->printf_json(array("params" => $params, "score" => $score));

        return $score;
    }

    private function printf_json ($params) {
        $filepath = "/www/webapps/logs/stg/".$this->router->fetch_class()."_".$this->router->fetch_method()."_".date("Ymd_H") .".log";
        $data['timestamp'] = date("Y/m/d H:i:s");
        $data['params'] = json_encode($params);
        file_put_contents($filepath, implode("  ", $data)."\n", FILE_APPEND);
    }
}
