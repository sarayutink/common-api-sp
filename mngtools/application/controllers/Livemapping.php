<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Livemapping extends CI_Controller {
    # "appid;langid;type;visitor;streamlvl;drm;channelid"
    // private $visitor = array('m'=>'mobile', 'web'=>'web', 'tv'=>'tv', 'antv'=>'antv', 'cc'=>'chromecast');

    public function map ($type) {
		$livemaps = json_decode($this->input->raw_input_stream, true);

        if (!is_null($livemaps)) {
            if ($livemaps['function'] == $type) {
                $this->load->model('livemap_model', 'redis');
                $this->redis->setParams();
    
                $set_arr = array();
                $findkeys = array('visitor' => array());
                foreach($livemaps['data'] as $data) {
                    foreach($data['visitor_list'] as $visitor_list) {
                        foreach($visitor_list['language_list'] as $language_list) {
                            foreach($language_list['list_smil'] as $list_smil) {
                                foreach($data['app_list'] as $app_list) {
                                    $key = implode(";", array($livemaps['app_id'], $language_list['language_code'], $type, $visitor_list['visitor_code'], $list_smil['bitrate'], $app_list['drm'], $data['channel_code']));
                                    $value = $app_list['instance_name'] ."/" . $list_smil['smil'];
                                    $set_arr[$key] = $value;
                                    if (!in_array($visitor_list['visitor_code'], $findkeys['visitor'])) $findkeys['visitor'][] = $visitor_list['visitor_code'];
                                }
                            }
                        }
                    }
                }
                // print_r($set_arr);
                // echo "count set_arr: ". count($set_arr) ."<br>";

                $result = $this->redis->setArray($set_arr, $livemaps['app_id'], $type, $findkeys['visitor']);
                $result['result'] = true;
                if ($result['result'] === true) $this->_output_json(array("result" => 200, "description" => "SUCCESS."));
                else $this->_output_json(array("result" => 500, "description" => $result['description']));
            }
            else $this->_output_json(array("result" => 500, "description" => "JSON value does not match."));
        }
        else $this->_output_json(array("result" => 500, "description" => "JSON structure is invalid."));
    }

    private function _output_json ($response_arr) {
        $logpath = "/www/logs/livemapping_". date("Ymd_H") .".log";
        $data['timestamp'] = date("Y/m/d H:i:s");
        $data['env'] = $this->uri->segment(2);
        $data['type'] = $this->uri->segment(3);
        $data += $response_arr;
        // $data['post'] = str_replace(["\n", " "], ["", ""], $this->input->raw_input_stream);
        file_put_contents($logpath, implode("  ", $data)."\n", FILE_APPEND);

        header('Content-Type: application/json');
        echo json_encode($response_arr);
    }
}
