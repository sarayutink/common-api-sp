<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cdnbalancer extends CI_Controller {


    public function node () {
        $postdata = $this->input->raw_input_stream;
        $extract = gzdecode($postdata);
        $msetarray = array();
        $setsarray = array();
        $nodecode = "";
        $i = 0;
        foreach (explode("\n", $extract) as $row) {
            ## 58.8.149.0/24,17552,TRUE,Broadband,AGN01,PTT,BMA-PTT,1605651,Asia,TH,TRUE_Broadband
            $cols = explode(",", $row);
            $msetarray[$cols[0]] = $cols[4];
            $setsarray[] = $cols[0];
            $nodecode = ($nodecode != $cols[4] ? $cols[4] : $nodecode);
        }

        $this->load->model('network_model', 'redis');
        $result = $this->redis->setNetwork($nodecode, $setsarray, $msetarray);
        if ($result['result'] == true) $return = array("result" => 200, "description" => "SUCCESS");
        else $return = array("result" => 200, "description" => $result['description']);

        $this->_output_json($return);
    }

    public function profile ($env) {
        $postdata = $this->input->raw_input_stream;
        $profile_arr = json_decode($postdata, true);
        $this->load->model('cdnprofile_model', 'redis');

        // $is_update_profile = true;
        if ($this->redis->getProfileVersion() == $profile_arr['profile_id']) $return = $this->redis->setPool($profile_arr);
        else $return = $this->redis->setProfile($profile_arr);
        // else $return = $this->redis->setProfile($profile_arr);

        // $return['result'] = $return['result'] == true ? 200 : 500;
        // $return['result'] = 200;
        // $return['description'] = $this->redis->getenv();
        $this->_output_json($return);
    }

    private function _output_json ($response_arr) {
        $logpath = dirname(dirname(FCPATH)). "/logs/". $this->uri->segment(1). "_". date("Ymd_H"). ".log";
        $data['timestamp'] = date("Y/m/d H:i:s");
        $data['env'] = $this->uri->segment(2);
        $data['method'] = $this->uri->segment(3);
        $data += $response_arr;
        $data['post'] = str_replace(["\n", " "], ["", ""], $this->input->raw_input_stream);
        file_put_contents($logpath, implode("  ", $data)."\n", FILE_APPEND);

        header('Content-Type: application/json');
        echo json_encode($response_arr);
    }
}